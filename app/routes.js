var
    Solution        = require('./models/solution'),
    path        = require('path')
;

module.exports = function(app) {

    // server routes ===========================================================
    // handle api calls

    // sample api route
    app.get('/api/solution', function(req, res) {
        // use mongoose to get all solutions in the database
        Solution.find(function(err, solutions) {

            // if there is an error retrieving, send the error.
            // nothing after res.send(err) will execute
            if (err)
                res.send(err);

            res.json(solutions); // return all solutions in JSON format
        });
    });

    app.get('/dat', function(req, res) {
        res.sendFile(path.join(__dirname, '../dat', 'OrderGroove.json'));
    });

    // route to handle creating goes here (app.post)
    app.post('/api/solution', function(req, res) {
        console.log('app.create placeholder req', req);
        res.json({message: 'app.create placeholder'});

    });
    // route to handle delete goes here (app.delete)
    app.delete('/api/solution', function(req, res) {
        console.log('app.delete placeholder req', req);
        res.json({message: 'app.delete placeholder'});

    });

    // frontend routes =========================================================
    // route to handle all angular requests

    app.get('*', function(req, res) {
        res.sendFile(path.join(__dirname, '../public', 'index.html'));
    });

};
