http://54.86.20.158:3001

### Exercise Details

Create a web app that contains a search box to retrieve information about people profiles matching their user names. 
Once the search is triggered show the user info or an error message if the search was not successful. 
For the user profiles data use the JSON provided and AJAX to get the file.

Create an autocomplete feature that suggest a list of users with similar username while you type on the search box.
Don't use jQuery or any third party library for DOM selection/manipulation. You can use libraries for data binding.

Unit tests not required, but certainly a bonus. For any need not specified use your own criteria.

-----

Unzip and install
    $ sudo npm install
    $ sudo bower install --allow-root
    $ node server.js

http://localhost:3001/

-----

Kill screens:
    $ screen -S MEANSTACK_LISTENER -X quit
    $ screen -S MONGODB_LISTENER -X quit
    
Start screens:
    $ screen -S MONGODB_LISTENER
    $ export PATH=/Users/todd/mongodb/bin:$PATH
        (use the actual path to your own Mongo install)
    $ sudo mongod
    [CTRL] + A followed by [CRTL] + D 
        (don't ask me why, this is how you detatch from SCREEN in terminal)
    $ screen -S MEANSTACK_LISTENER
    $ cd /prweb/dev
    $ grunt 
    
Detach screen: 
    [CTRL]+a -> [CTRL]+d
    
Re-attach screens:
    $ screen -R MEANSTACK_LISTENER_FQ
    $ screen -R MONGODB_LISTENER

-----

Source Control    
    $ git init
    $ git remote add origin git@bitbucket.org:tbollack/og.git
    $ git add . --all
    $ git commit -am "inital commit"
    $ git push --set-upstream origin master