angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'MainController'
            })

        // solutions page that will use the SolutionController
            .when('/solution', {
                templateUrl: 'views/solution.html',
                controller: 'SolutionController'
            });

    $locationProvider.html5Mode(true);

}]);