angular.module('SolutionService', []).factory('Solution', ['$http', function($http) {

    return {
        // call to get all solutions
        get : function() {
            return $http.get('/api/solution');
        },

        // calls to API route placeholders
        // call to POST and create a new solution
        create : function(solutionData) {
            return $http.post('/api/solution', solutionData);
        },

        // call to DELETE a solution
        delete : function(id) {
            return $http.delete('/api/solution/' + id);
        }
    }

}]);