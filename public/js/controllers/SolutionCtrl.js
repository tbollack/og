angular.module('SolutionCtrl', []).controller('SolutionController', function($scope, $timeout) {
    $scope.showTypeAhead = false;
    $scope.strTerm = '';
    $scope.objActiveUser = {};
    $scope.objError = {message:'ok'};
    $scope.arrSearchResult = [];
    $scope.objCallback = null;
    $scope.intTotalItems = 0;
    $scope.intCurrentPage = 1;
    $scope.intItemsPerPage = 10;

    $scope.setPagination = function(){
        console.log('$scope.intCurrentPage', $scope.intCurrentPage);
    };

    $scope.searchData = function(){
        $scope.objError = {message:'ok'};
        $scope.showTypeAhead = false;
        $scope.objActiveUser = {};
        $scope.arrSearchResult = [];

        for(var intUserIndex = 0; intUserIndex < $scope.arrUsers.length; intUserIndex++){
            var
                    objUser = $scope.arrUsers[intUserIndex],
                    strTerm = $scope.strTerm.toLowerCase();
            if(
                    objUser.username.toLowerCase().indexOf(strTerm) > -1 ||
                    objUser.first_name.toLowerCase().indexOf(strTerm) > -1 ||
                    objUser.last_name.toLowerCase().indexOf(strTerm) > -1 ||
                    objUser.email.toLowerCase().indexOf(strTerm) > -1
            ){
                $scope.arrSearchResult.push(objUser);
            }
        }

        if($scope.arrSearchResult.length > 0){
            $scope.objError = {message:'ok'};
            $scope.intTotalItems = $scope.arrSearchResult.length;
            $scope.intCurrentPage = 1;
        }else{
            $scope.objError = {message:'No users matched the input criteria.'};
        }
    };

    $scope.showUser = function(objUser){
        $scope.objActiveUser = objUser;
        $scope.showTypeAhead = false;
        $scope.arrSearchResult = [];
        $scope.objError = {message:'ok'};
    };

    $scope.onTextChange = function(){
        $scope.showTypeAhead = ($scope.strTerm.length >= 2);
    };

    $scope.onTextBlur = function(){
        $scope.objCallback = $timeout(function(){
            $scope.showTypeAhead = false;
        }, 200);
    };

    $scope.xhrRequest = function(strURL){
        var objXHR = new XMLHttpRequest();
        objXHR.responseType = 'json';
        objXHR.open('GET', strURL);
        objXHR.onreadystatechange = function(){
            if (objXHR.readyState === 4 && objXHR.status === 200) {
                $scope.arrUsers = objXHR.response.sort(function(a, b){
                    return a.username === b.username ? 0 : +(a.username > b.username) || -1;
                });
            }
        };
        objXHR.send();
    };

    $scope.initApp = function(){
        $scope.xhrRequest('/dat');
    };

});